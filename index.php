<?php
  $link = 'http://api.openweathermap.org/data/2.5/weather';
  $apiKey = 'e355704a95bf17365dabbacffce37ad0';
  $city = 'Almaty';
  $units = 'metric';
  $apiURL = "{$link}?q={$city}&units={$units}&appid={$apiKey}";
  $api_weather_data = file_get_contents($apiURL) or exit('Не удалось получить данные');
  $response = json_decode($api_weather_data, true);
  if ($response === null) {
    exit('Ошибка декодирования json');
  }
 
  $srcIconURL = 'http://openweathermap.org/img/w/';
  $weather = [
    'id' => $response['weather'][0]['id'],
    'main' =>  $response['weather'][0]['main'],
    'icon' => $response['weather'][0]['icon'],
    'temp' => $response['main']['temp'],
    'pressure' => $response['main']['pressure'],
    'humidity' => $response['main']['humidity'],
    'windSpeed' => $response['wind']['speed'],
    'city' => $response['name']
  ];
?>

<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="https://fonts.googleapis.com/css?family=Oswald:400,700" rel="stylesheet">
  <link rel="stylesheet" href="main.css">
</head>
<body>
  <div class="wrap">
    <h1 class="city-name"><?= (!empty($weather['city'])) ? $weather['city'] : 'Не удалось получить город' ?></h1>
    <div class="main-info">
      <div class="weather-image">
        <img src="<?= (!empty($weather['icon'])) ? $srcIconURL . $weather['icon'] . '.png' : './img/error.svg' ?>" alt="">
      </div>
      <p class="temp"><?= (!empty($weather['temp'])) ? $weather['temp'] : 'Не удалось получить температуру' ?>&#176;</p>
    </div>
    <div class="additional-info">
      <div class="pressure">
        <img class="additional-img" src="./img/pressure.svg" alt="">
        <p class="additional-text"><?= (!empty($weather['pressure'])) ? $weather['pressure'].'mb' : 'Не удалось получить давление' ?></p>
      </div>
      <div class="wind">
        <img class="additional-img" src="./img/windmill.svg" alt="">
        <p class="additional-text"><?= (!empty($weather['windSpeed'])) ? $weather['windSpeed'].'m/s ' : 'Не удалось получить скорость ветра' ?></p>
      </div>
      <div class="humidity">
        <img class="additional-img" src="./img/humidity.svg" alt="">
        <p class="additional-text"><?= (!empty($weather['humidity'])) ? $weather['humidity'].'%' : 'Не удалось получить влажность' ?></p>
      </div>
    </div>
  </div>
</body>
</html>